--[[
    The NavigationBar is a node that acts as a input and selector at the same time. Buttons showing the path the user is currently in can be seen
    inside the Navigation bar, the buttons can be clicked to change to that directory.

    If a click occurs on whitespace, rather than a button the input is toggled allowing the user to type a path or edit the one the user is currently in.
]]
class NavigationBar extends Container {
    sections = {};
}

--[[
    @constructor
    @desc Constructs the contents of the navigation bar. Among other things, this includes a page container and the two pages used to swap between input and buttons.
]]
function NavigationBar:__init__( ... )
    self:super( ... )
    self.consumeAll = false;

    self.pages = self:addNode( PageContainer( 1, 1, 1, 1 ):set{ backgroundColour = self.backgroundColour, consumeAll = false, width = "$parent.width" } )
    self.pages.customAnimation = function( _, page ) self.pages.scroll = page.X - 1 end

    self.buttonPage, self.addressPage = Page "buttons":set( "consumeAll", false ), Page "address":set( "consumeAll", false )

    self.pages:addPage( self.buttonPage )
    self.pages:addPage( self.addressPage )
    self.addressInput = Input( 1, 1 ):on("trigger", function( this, value )
        if fs.isDir( value ) then self:hideAddressBar() end
        self.application:goToDirectory( value )
    end):set{ backgroundColour = 1, width = "$parent.width" }

    self.addressPage:addNode( self.addressInput )
    self.pages:selectPage "buttons"
end

--[[
    @instance
    @desc Returns a table containing the absolute path of the sections selected. Selection is done by finding how many section names can fit inside the limit given, starting at the end (the last 'n' sections)
    @param <number - limit>
    @return <table - sections>
]]
function NavigationBar:getSectionsToFit( limit )
    if type( limit ) ~= "number" then
        return error("Failed to calculate sections to fit. Limit '"..tostring( limit ).."' is invalid, number required")
    end

    local sections, sectionReturn, width = Plexus.static.splitPathAbsolute( self.application.location ), {}, 0
    for i = 1, #sections do
        local name = fs.getName( sections[ i ] )
        if #name + width + 1 > limit then break end

        table.insert( sectionReturn, 1, sections[ i ] )
        width = width + #name + 1
    end

    return sectionReturn
end

--[[
    @instance
    @desc Updates the buttons displayed inside of the button page by getting the sections that can fit in the space (width - 5).
]]
function NavigationBar:updateSectionListing()
    local parent = self.buttonPage
    parent:clearNodes()

    local sections, x = self:getSectionsToFit( self.width - 5 ), 1
    for i = 1, #sections do
        local name = fs.getName( sections[ i ] )
        parent:addNode( Button( name, x, 1, #name ):on("trigger", function() self.application:goToDirectory( sections[ i ] ) end):set{ activeBackgroundColour = 256, activeColour = colours.blue } )

        if i < #sections then parent:addNode( Label( "/", x + #name ) ) end
        x = x + #name + 1
    end
end

--[[
    @instance
    @desc Shows the address bar by changing page. The input is also focused and all text inside is selected for easy replacement
]]
function NavigationBar:showAddressBar()
    if self.pages.selectedPage.id == "address" then return end
    self.pages:selectPage "address"

    local input = self.addressPage.nodes[ 1 ]
    input.value, input.selection = self.application.location, 0
    input.position = #input.value

    input:focus()
end

--[[
    @instance
    @desc Hides the address bar by changing page, revealing the navigation buttons. The address bar input is also unfocused
]]
function NavigationBar:hideAddressBar()
    self.pages:selectPage "buttons"
    self.addressInput:unfocus()
end

--[[
    @instance
    @desc Intercepts the containers handle function, used to determine if click landed on a navigation button or whitespace (toggles page depending on this)
    @param <Event Instance - eventObj>
    @return <boolean - propagate>
]]
function NavigationBar:handle( eventObj )
    local r = self.super:handle( eventObj )
    if eventObj.main ~= "MOUSE" then return end

    if eventObj:withinParent( self ) then
        if not eventObj.handled then
            self:showAddressBar()
        end
    else self:hideAddressBar() end

    return r
end
