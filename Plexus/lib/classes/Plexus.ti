class Plexus extends Application mixin MSelectionManager {
    static = {
        mimeTypes = {},
        fileBlacklist = { ".DS_Store" },
        deviceWhitelist = { "computer", "turtle", "drive" },
        allowedFlags = { location = true, selector = true, nosidebar = true, notitle = true, noclose = true, nofooter = true, ["view-blacklisted"] = true, nosize = true }
    };
    history = {
        offset = 0;
        entries = {};
        testing = {};
    };
    sorting = {
        target = false;
        reverse = false;
    };
    flags = {};

    peripherals = {};
    location = "/";
    computerID = false;
}

--[[
    @constructor
    @desc WIP
]]
function Plexus:__init__()
    self.computerID = computerID
    self:super( "/", 1, 1, term.getSize() )
end

--[[
    @instance
    @desc Prepares the Plexus instance to begin. Caches important nodes, information, mounts peripherals and sets up UI. Starts the application afterwards
]]
function Plexus:start( ... )
    self:parseArguments( ... )
    self:modifyUI()

    if type( _PLEXUS_START ) == "function" then
        _PLEXUS_START( self )
    end

    self.cache = {
        listing_display = self:query "#listing_container".result[ 1 ],
        content_container = self:query ".content_cont".result[ 1 ],
        sidebar = self:query "ScrollContainer#sidebar".result[ 1 ],
        name_header = self:query "Button#name_header".result[ 1 ],
        size_header = self:query "Button#size_header".result[ 1 ],
        nav = self:query "NavigationBar".result[ 1 ]
    }

    self:sortBy "Name"
    self:updateListing()
    self:addToHistory( self.location )
    self.super:start()
end

--[[
    @instance
    @desc WIP
]]
function Plexus:parseArguments( ... )
    local args, i = { ... }, 1

    while i <= #args do
        local arg = args[ i ]
        if arg:match "^%-%-[%w%-]*$" then
            local flag = arg:match "^%-%-([%w%-]*)$"
            if not Plexus.static.allowedFlags[ flag ] then error("Unknown flag '"..arg.."'")
            elseif flag == "location" then
                i = i + 1
                self.location = args[ i ] or error("Found location flag with no location following")
            else self.flags[ flag ] = true end
        else
            error("Invalid argument found: " .. tostring( arg )..". Must be of format '--arg'")
        end

        i = i + 1
    end
end

--[[
    @instance
    @desc Removes/inserts elements of the UI depending on the flags provided (ie: --notitle, --nosidebar, --selector, etc)
]]
function Plexus:modifyUI()
    local flags = self.flags
    local isSelector = self.flags.selector
    if flags.nosidebar then
        self:query "#control_bar > Button#sidebar_toggle, ScrollContainer#sidebar":remove()
        self:query "#control_bar > NavigationBar".result[ 1 ].width = 35
    end

    if flags.noclose then self:query "Button#exit":remove() end

    if flags.notitle then
        self:query "#control_bar > Label[text == Plexus]":remove()
        self:query "#control_bar":set( "height", 3 )
    end

    if isSelector then
        self:query ".content_cont".result[ 1 ]:importFromTML "Plexus/ui/selector.tml"
        self:query "Button#selection_cancel":on("trigger", function() self:executeCallbacks( "cancel" ) end )
        self:query "Button#selection_confirm":on("trigger", function() self:executeCallbacks( "confirm" ) end )
    end

    if flags.nofooter then
        self:query "Label#footer":remove()
        self:query "ScrollContainer#listing_container".result[ 1 ].height = "$parent.height - " .. ( isSelector and "4" or "1" )
        self:query "#selector":set( "Y", "$parent.height - self.height + 1" )
    elseif isSelector then
        self:query "ScrollContainer#listing_container".result[ 1 ].height = "$parent.height - 5"
    end

    if flags.nosize then
        self:query "#size_header":set { visible = false, enabled = false }
    end
end

--[[
    @instance
    @desc Animates the content of the Plexus instance to reveal the sidebar. Adds the 'toggled' class to the sidebar
]]
function Plexus:revealSidebar()
    self.cache.sidebar:addClass "toggled"
    self.cache.content_container:animate("contentX", "X", 1, 0.1, "outSine")
end

--[[
    @instance
    @desc Animates the content of the Plexus instance to hide the sidebar. Removes the 'toggled' class to the sidebar
]]
function Plexus:hideSidebar()
    self.cache.sidebar:removeClass "toggled"
    self.cache.content_container:animate("contentX", "X", -13, 0.15 )
end

--[[
    @instance
    @desc Toggles he visibility of the sidebar. If the sidebar contains the class of 'toggled',
          it will be hidden. If the class is not applied, it will be revealed.
]]
function Plexus:toggleSidebar()
    if self.cache.sidebar:hasClass "toggled" then
        self:hideSidebar()
    else
        self:revealSidebar()
    end
end

--[[
    @instance
    @desc Displays the current working directory contents inside the Plexus file listing container.
          Directories are listed first, followed by files.
    @param [string - location], [boolean - preserve]
]]
function Plexus:updateListing( loc, preserve )
    local listing, pwd = self.cache.listing_display, self.location
    if not preserve then listing.nodes = {} end

    local blacklist, useBlacklist, selector = Plexus.static.fileBlacklist, self.flags["view-blacklisted"], self.flags.selector
    for i, path in pairs( TI_VFS_RAW.fs.list( loc or self.location ) ) do
        local fullPath = fs.combine( pwd, path )
        local isDir = fs.isDir( fullPath )

        if ignoreBlacklist or not util.isInTable( blacklist, path ) then
            local node = ( Titanium.getClass( isDir and "DirectoryListing" or "FileListing" ) )( self, fullPath ):on("trigger", function()
                if isDir then
                    self:goToDirectory( fullPath )
                else
                    local ok, err = loadfile "/rom/programs/edit"
                    if not ok then
                        error("Failed to load /rom/programs/edit: " .. tostring( err ))
                    end

                    ok( fullPath )
                    self:draw( true )
                end
            end)

            listing:addNode( node )
            node.checkbox:updateState()
        end
    end

    self:sortListing()
    listing.yScroll = 0

    self.cache.nav:updateSectionListing()
    self:alignSizeInformation()

    self.changed = true
end

--[[
    @instance
    @desc WIP
]]
function Plexus:sortListing()
    local sorting, listing, selector = self.sorting, self.cache.listing_display, self.flags.selector
    table.sort( listing.nodes, function( a, b )
        local main = not sorting.reverse and a or b
        local sub = main == a and b or a

        if main.__type == sub.__type then
            if sorting.target == "Name" or not main.cache.size then
                return main.fileName:lower() < sub.fileName:lower()
            elseif sorting.target == "Size" then
                return main.cache.size < sub.cache.size
            end
        else
            if sorting.target == "Size" then
                return not sorting.reverse and main.__type == "FileListing"
            else
                return main.__type == "DirectoryListing"
            end
        end
    end )

    for i = 1, #listing.nodes do listing.nodes[ i ].Y = selector and 1 + ( ( i - 1 ) * 3 ) or i end
    listing:cacheContent()
end

--[[
    @instance
    @desc WIP
]]
function Plexus:sortBy( target )
    local sorting = self.sorting
    if sorting.target == target then
        sorting.reverse = not sorting.reverse
    else
        sorting.target = target
        sorting.reverse = false
    end

    self.cache[ sorting.target == "Name" and "name_header" or "size_header" ].text = sorting.target .. " " .. ( sorting.reverse and "\31" or "\30" )
    self.cache[ sorting.target == "Name" and "size_header" or "name_header" ].text = sorting.target == "Name" and "Size" or "Name" .. " "

    self:sortListing()
end

--[[
    @instance
    @desc Finds the maximum file name in the listing. This acts as the columns width and is used to position the size column.
]]
function Plexus:alignSizeInformation()
    local nameWidth = math.max( util.getMaxLength( self.cache.listing_display.nodes, "fileName" ) + 4, 30 )

    self.margin = nameWidth
    self.cache.size_header.X = nameWidth
end

--[[
    @instance
    @desc WIP
]]
function Plexus:addToHistory( path )
    local histPosition, pointInHist = self.history.offset, #self.history.entries + self.history.offset
    if histPosition < 0 then
        self.history.entries[ pointInHist + 1 ] = path
        for i = pointInHist + 2, #self.history.entries do
            table.remove( self.history.entries, i )
        end
        self:historyNavigate(0)
    else
        table.insert( self.history.entries, path )
    end
end

--[[
    @instance
    @desc Navigate through the Plexus instance history
    @param <number - direction>

    Note: If number (direction) is positive, attempts will be made to move forward through the history. If number is negative, the opposite holds true.
]]
function Plexus:historyNavigate( offset )
    local hOffset, offset = self.history.offset, offset or 0

    if offset > 0 and hOffset == 0 then
        return error "Attempt to navigate forward past history end point"
    elseif offset < 0 and hOffset == #self.history.entries*(-1) then
        return error "Attempt to navigate backward past history start point"
    elseif offset == 0 then
        self.history.offset = 0
    else
        self.history.offset = hOffset + offset
    end
end

--[[
    @instance
    @desc Used to change the Plexus instance location, and update the listing
    @param <string - path>
]]
function Plexus:goToDirectory( path, omitHistory )
    self.location = path
    self:updateListing()
    if not omitHistory then self:addToHistory( path ) end
end

--[[
    @instance
    @desc Navigates to the parent directory of the current location if one exists.
]]
function Plexus:navigateUp()
    local target = Plexus.static.cleanPath( self.location:match "(.*[^/]+)/[^/]*$" or "" )

    if target == self.location then return end
    self:goToDirectory( target )
end

--[[
    @setter
    @desc Sets the instances location to a clean version of the path
    @param <string - pwd>
]]
function Plexus:setLocation( pwd )
    local cleaned = Plexus.static.cleanPath( pwd )
    if not ( TI_VFS_RAW.fs.exists( pwd ) and TI_VFS_RAW.fs.isDir( pwd ) ) then
        local diag = DialogWindow( self.width / 2 - 12.5, self.height / 2 - 3, 25, 8, "Invalid Location", "\nThe location provided ("..pwd..") is invalid." )
        diag:addNode( Button("Ok"):set {
            width = 6,
            horizontalAlign = "centre",
            backgroundColour = 256,
            colour = 128
        } ):on("trigger", function()
            self:removeDialog( diag )
        end)

        self:addDialog( diag )
    else
        self.location = pwd
    end
end

--[[
    @instance
    @desc Adds currently connected, whitelisted devices to the peripherals table.
]]
function Plexus:discoverPeripherals()
    local whitelist = Plexus.static.deviceWhitelist

    self.peripherals = {}
    for i = 1, #whitelist do
        local peripherals = { peripheral.find( whitelist[ i ] ) }
        for i = 1, #peripherals do
            table.insert( self.peripherals, peripherals[ i ] )
        end
    end
end

--[[
    @static
    @desc Cleans a file system path by removing anomolies that will likely cause problems when using the filesystem commands
    @param <string - path>
    @return <string - path>
]]
function Plexus.static.cleanPath( path )
    return path
            :gsub("^%./", "") -- Remove trailing ./
            :gsub("/%./", "/") -- Replace ./ inside path with /
            :gsub("\\", "/") -- Replace \ with /
            :gsub("//+", "/") -- Remove duplicate slashes
            :gsub("/+$", "") -- Remove trailing slashes
            :gsub("^/", "") -- Remove leading slashes
end

--[[
    @static
    @desc 'Explode' the path provided into 'parts' that represent directories
    @param <string - uncleanPath>
    @return <table - parts>, <string - cleanPath>
]]
function Plexus.static.splitPath( path )
    local parts, path = {}, Plexus.static.cleanPath( path )
    for part in path:gmatch "([^/]+)/?" do
        parts[ #parts + 1 ] = part
    end

    return parts, path
end

--[[
    @instance
    @desc Functions very similarly to splitPath, however each part of the path is absolute rather than just a name.

          For example, a path of /hello/foo/world/bar would be split into '/hello/foo/world/bar', '/hello/foo/world',
          '/hello/foo', '/hello' meaning the path can still actually be used (isn't just 'world' or 'foo').
    @param <string - uncleanPath>
    @return <table - absoluteParts>, <string - cleanPath>
]]
function Plexus.static.splitPathAbsolute( path )
    local parts, path = {}, Plexus.static.cleanPath( path )
    while path:len() > 0 do
        parts[ #parts + 1 ] = path
        path = path:gsub( "/?[^/]-$", "" )
    end

    return parts, path
end

--[[
    @static
    @desc Returns the final part of the extension for 'path'
    @param <string - cleanPath>
    @return <string - extension>
]]
function Plexus.static.getExtension( path )
    return path:match ".+%.(.-)$" or ""
end

--[[
    @static
    @desc Registers mime type 'mime', with the extensions provided. Previous extensions bound to mime type are preserved
    @param <string - mime>, [string (vararg) - extensions]

    Note: 'extensions' represents the file extensions to associate with this mime type
]]
function Plexus.static.registerMime( mime, ... )
    local extensions, existing, ext = { ... }, Plexus.mimeTypes[ mime ] or {}
    for i = 1, #extensions do
        ext = extensions[ i ]
        if not util.isInTable( existing, ext ) then
            existing[ #existing + 1 ] = ext
        end
    end

    Plexus.mimeTypes[ mime ] = existing
end

--[[
    @static
    @desc Returns the mime type associated with the extension of 'path'.
    @param <string - path>
    @return [string - mime]
]]
function Plexus.static.determineMime( path )
    local ext, mimes = Plexus.static.getExtension( path ), Plexus.mimeTypes
    for mime, exts in pairs( mimes ) do
        for e = 1, #exts do
            if exts[ e ] == ext then
                return mime
            end
        end
    end
end

--[[
    @static
    @desc Adds the input device type to the deviceWhitelist table.
    @param <string - device>
]]
function Plexus.static.whitelistDevice( device )
    if type( device ) == "string" then
        table.insert( Plexus.static.deviceWhitelist, device )
    else
        return error "Failed to whitelist peripheral. Invalid peripheral type provided"
    end
end

configureConstructor {
    orderedArguments = {"location"},
    requiredArguments = {"location"},
    useProxy = {"location"},
    argumentTypes = {
        location = "string"
    }
}
