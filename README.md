Plexus
======

[![build status](https://gitlab.com/Kamakaze0001/Plexus/badges/develop/build.svg)](https://gitlab.com/Kamakaze0001/Plexus/commits/develop)
[![Language (Lua)](https://img.shields.io/badge/powered_by-Lua-blue.svg?style=flat)](https://lua.org)
[![License (MIT)](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](http://opensource.org/licenses/MIT)
[![Gitter](https://badges.gitter.im/hbomb79/Titanium.svg)](https://gitter.im/hbomb79/Titanium?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

A modular, configurable file browser for ComputerCraft.

##### Download
Available via [Titanium package manager](https://gitlab.com/hbomb79/Titanium-Package-Manager) using command `tpm install Plexus`. Use `tpm help` to learn more about installing packages.
